var cluster = require('cluster');

var redisApp = require("../app.js");
var http = require('http');
var https = require('https');
var fs = require('fs');
var pem = require('pem');
var debug = require('debug')('testTask:server');
var bodyParser = require('body-parser');

/**
 * SSL creditentials for connection to HTTPS
 */

if (cluster.isMaster) {
    // Count the machine's CPUs
    var cpuCount = require('os').cpus().length;

    // Create a worker for each CPU
    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }
} else {
    var express = require('express');
    var httpApp = express();
    var app = express();

    /**
     * for processing request body
     */
    app.use(bodyParser.json());       // to support JSON-encoded bodies
    app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
        extended: true
    }));

    var privateKey = fs.readFileSync('/home/nerd/Downloads/redis-3.0.7/src/key.pem', 'utf8');
    var certificate = fs.readFileSync('/home/nerd/Downloads/redis-3.0.7/src/cert.pem', 'utf8');
    var httpsOptions = {key: privateKey, cert: certificate};

    /**
     * setting up the ports for http server and https server
     */
    var port = normalizePort(process.env.PORT || 80);
    httpApp.set("port", process.env.PORT || port);
    app.set('port', process.env.PORT || 443);

    /**
     * create http server and redirecting url to https
     * 307 -    is a status code (307) which indicates that
     *          the request should be repeated using the same method
     *          and post data
     */
    http.createServer(httpApp).listen(httpApp.get('port'), function () {
        httpApp.post("/api/get-data/", function (req, res) {
            res.redirect(307, "https://" + req.headers.host + req.path);

        });
        console.log('Express HTTP server listening on port ' + httpApp.get('port'));
    });

    /**
     * creating HTTPS server and exporting it to operate with redis application
     * which gets data from redis db
     * @type {http.Server}
     */
    module.exports.httpsServer = https.createServer(httpsOptions, app).listen(app.get('port'), function () {
        app.post("/api/get-data/", function (req, res) {
            if (req.headers.sig) {
                redisApp.getIdByEmail(req.body.email, function (userId) {
                    if (userId) {
                        redisApp.getClient().get('users:' + userId, function (request, response) {
                            res.send(JSON.stringify({result: true, userId: userId}))
                        });
                    }
                })
            } else {
                res.send(JSON.stringify({result: false}))
            }
        });
        console.log('Express HTTPS server listening on port ' + app.get('port'));
    });


    httpApp.on('error', onError);
    httpApp.on('listening', onListening);

    /**
     * Normalize a port into a number, string, or false.
     */

    function normalizePort(val) {
        var port = parseInt(val, 10);

        if (isNaN(port)) {
            // named pipe
            return val;
        }

        if (port >= 0) {
            // port number
            return port;
        }

        return false;
    }

    /**
     * Event listener for HTTP server "error" event.
     */

    function onError(error) {
        if (error.syscall !== 'listen') {
            throw error;
        }

        var bind = typeof port === 'string'
            ? 'Pipe ' + port
            : 'Port ' + port;

        // handle specific listen errors with friendly messages
        switch (error.code) {
            case 'EACCES':
                console.error(bind + ' requires elevated privileges');
                process.exit(1);
                break;
            case 'EADDRINUSE':
                console.error(bind + ' is already in use');
                process.exit(1);
                break;
            default:
                throw error;
        }
    }

    /**
     * Event listener for HTTP server "listening" event.
     */

    function onListening() {
        var addr = httpApp.address();
        var bind = typeof addr === 'string'
            ? 'pipe ' + addr
            : 'port ' + addr.port;
        debug('Listening on ' + bind);
    }

}

module.exports = app;

var redis = require('redis');
var client = redis.createClient(); //creates a new client

var path = require('path');
var routes = require('./routes/index');
var users = require('./routes/users');

var express = require('express');
var clApp = express();
client.on('connect', function () {
    console.log('connected');
});

/** get user id by email  **/
module.exports.getIdByEmail = function (email, callback) {
    return client.hget('users:lookup:email', email, function (err, userId) {
        if (callback)
            callback(userId)
    });
};

/*module.exports.getAllKeys = function () {
 var dbKeys = [];
 client.keys('*', function (err, keys) {
 if (err) return console.log(err);

 for (var i = 0, len = keys.length; i < len; i++) {

 dbKeys.push(keys[i]);
 }
 });
 console.log("keys in redis DB",dbKeys)
 return dbKeys;
 };*/


// view engne setup
clApp.set('views', path.join(__dirname, 'views'));
clApp.set('view engine', 'jade');
clApp.use(express.static(path.join(__dirname, 'public')));
clApp.use('/', routes);
clApp.use('/users', users);

// catch 404 and forward to error handler
clApp.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (clApp.get('env') === 'development') {
    clApp.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
clApp.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

this.getClient = function () {
    return client;
};


